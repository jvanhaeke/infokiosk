/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.katho.infokiosk.dal;

import be.katho.infokiosk.model.Category;
import be.katho.infokiosk.model.Product;
import org.hibernate.classic.Session;

/**
 *
 * @author Jonas
 */
public class ProductDAO {
    public Product getProductById(int id) {
        Session session = infokioskUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Product result = (Product) session.get(Product.class, id);
        session.getTransaction().commit();
        session.close();
        return result;            
    }
    
    public Category getCategoryById(int id){
        Session session = infokioskUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Category result = (Category) session.get(Category.class, id);
        session.getTransaction().commit();
        session.close();
        return result;
    }
}
