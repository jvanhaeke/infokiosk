/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.katho.infokiosk.controller;

import be.katho.infokiosk.dal.infokioskUtil;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author Jonas
 */
public class ConnectionListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent event) {
        infokioskUtil.getSessionFactory(); 
    }
 
	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
        infokioskUtil.getSessionFactory().close();
    }
}
