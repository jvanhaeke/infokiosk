/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.katho.infokiosk.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author Jonas
 */

@Entity
@Table(name="details", catalog="centricinfokiosk")
public class ProductDetail implements Serializable {
    @Id
    @Column(name="Id")
    @GeneratedValue
    private Integer detailId;
    
    @Column(name="Detail")
    private String key;
    
    //private String value;
    
    @ManyToMany(mappedBy="details")
    private Set<Product> products = new HashSet<Product>();

    public ProductDetail() {
    }

    public ProductDetail(Integer detailId) {
        this.detailId = detailId;
    }

    public ProductDetail(Integer detailId, String key) {
        this.detailId = detailId;
        this.key = key;
    }
    
    public Integer getDetailId() {
        return detailId;
    }

    public void setDetailId(Integer detailId) {
        this.detailId = detailId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
    
    
}
