/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.katho.infokiosk.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author Jonas
 */
@Entity
@Table(name="products", catalog="centricinfokiosk")
public class Product implements java.io.Serializable{
    @Id
    @Column(name="Id", unique=true, nullable=false,length=64)
    @GeneratedValue
    private Integer id;
    
    @Column(name="Name", length=45)
    private String name;
    
    @Column(name="Description", length=200)
    private String description;
     
    @Column(name="Price")
    private double price;
    
    @ManyToOne
    @JoinColumn(name="Categories_Id")
    private Category category;
    
    @ManyToMany(fetch = FetchType.EAGER,cascade = {CascadeType.ALL})
    @JoinTable(name="productmediaitems", 
                joinColumns={@JoinColumn(name="Products_Id")}, 
                inverseJoinColumns={@JoinColumn(name="MediaItems_Id")})
    private Set<MediaItem> mediaitems = new HashSet<MediaItem>();
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="products")
    private Set<Productdetail> productdetails = new HashSet<Productdetail>();
    
    public Product()
    {
        
    }

    public Product(int id) {
        this.id = id;
    }

    public Product(Integer id, String name, String description, double price, Category category) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.category = category;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public MediaItem getFirstPicture(){
        return (MediaItem)mediaitems.toArray()[0];
    }
    
    public Set<MediaItem> getMediaitems() {
        return mediaitems;
    }

    public void setMediaItems(HashSet<MediaItem> mediaItems) {
        this.mediaitems = mediaItems;
    }
    
    public Set<Productdetail> getProductdetails() {
        return this.productdetails;
    }
    
    public void setProductdetails(Set productdetails) {
        this.productdetails = productdetails;
    }
    
}
