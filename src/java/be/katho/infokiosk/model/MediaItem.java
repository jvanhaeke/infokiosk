/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.katho.infokiosk.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Jonas
 */
@Entity
@Table(name="mediaitems", catalog="centricinfokiosk")
public class MediaItem implements Serializable {
    @Id
    @Column(name="Id", unique=true, nullable=false,length=64)
    @GeneratedValue
    private Integer id;
     
    @Column(name="Description", length=45)
    private String description;
    
    @Column(name="Url", length=255)
    private String url;
    
    @ManyToMany(mappedBy="mediaitems")
    private Set<Product> products = new HashSet<Product>();

    public MediaItem() {
    }

    public MediaItem(Integer id) {
        this.id = id;
    }

    public MediaItem(Integer id, String description, String url) {
        this.id = id;
        this.description = description;
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
    
    
    
    
}
