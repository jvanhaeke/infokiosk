package be.katho.infokiosk.model;
// Generated 8-nov-2012 14:15:18 by Hibernate Tools 3.2.1.GA


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ProductdetailsId generated by hbm2java
 */
@Embeddable
public class ProductdetailsId  implements java.io.Serializable {

    @Column(name="Details_Id", nullable=false)
     private int detailsId;
    @Column(name="Products_Id", nullable=false)
     private int productsId;

    public ProductdetailsId() {
    }

    public ProductdetailsId(int detailsId, int productsId) {
       this.detailsId = detailsId;
       this.productsId = productsId;
    }
   


    public int getDetailsId() {
        return this.detailsId;
    }
    
    public void setDetailsId(int detailsId) {
        this.detailsId = detailsId;
    }

        public int getProductsId() {
        return this.productsId;
    }
    
    public void setProductsId(int productsId) {
        this.productsId = productsId;
    }

}


