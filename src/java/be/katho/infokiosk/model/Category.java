/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.katho.infokiosk.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author Jonas
 */
@Entity
@Table(name="categories", catalog="centricinfokiosk")
public class Category implements java.io.Serializable{
    @Id
    @Column(name="Id", unique=true, nullable=false,length=64)
    private String id;
    
    @Column(name="Name", length=45)
    private String name;
    
    @ManyToOne(fetch = FetchType.EAGER, cascade={CascadeType.ALL})
    @JoinColumn(name="Categories_Id")
    private Category headCategory;
    
    @OneToMany(mappedBy="headCategory")
    private Set<Category> subCategories = new HashSet<Category>();
    
    public Category() {
    }

    public Category(String id) {
        this.id = id;
    }

    public Category(String id, String name, Category headcategory) {
        this.id = id;
        this.name = name;
        this.headCategory = headcategory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getHeadCategory() {
        return headCategory;
    }

    public void setHeadCategory(Category headCategory) {
        this.headCategory = headCategory;
    }

    public Set<Category> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(Set<Category> subCategories) {
        this.subCategories = subCategories;
    }
    
    
}
