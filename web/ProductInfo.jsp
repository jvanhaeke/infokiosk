<%-- 
    Document   : ProductInfo
    Created on : 25-okt-2012, 14:17:24
    Author     : Jennis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <link rel=StyleSheet href="CSS/ProductInfoCSS.css" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Productinformatie</title>
    </head>
    <body>
        <div id="Leftside">
        <div id="ImageHolder">
            <p>
                <img src="${viewproducts.getFirstPicture().getUrl()}" alt="${viewproducts.getFirstPicture().getDescription()}"/>
            </p>
        </div>
            <div id="thumbnails"></div>
        </div>
        <div id="ProductInformation"> 
         <h1> ${viewproducts.getName()}</h1><hr/>
         <h3>${viewproducts.getDescription()}</h3>
         <ul>
           <c:forEach items="${viewproducts.getProductdetails()}" var="Detail">
               <li>
                   ${Detail.detail} : ${Detail.values}
                   
               </li>
           </c:forEach>
       </ul> 
           <h3>Onderdeel van ${viewproducts.getCategory().getName()}</h3>
           <h2> € ${viewproducts.getPrice()}</h2>
               
        </div>
           
           
    </body>
</html>
